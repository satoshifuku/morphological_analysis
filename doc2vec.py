# -*- coding: utf-8 -*-

import sys
import MeCab
import gensim
from gensim.models.doc2vec import Doc2Vec
from gensim.models.doc2vec import TaggedDocument
from collections import OrderedDict
import pathlib
import numpy as np
import pandas as pd

import extract_text

from gensim.similarities import MatrixSimilarity

import pickle

def main():
    args = sys.argv
    making_model = True
    if len(args) > 1:
        for i in range(1, len(args)):
            if args[i] == '-m':
                making_model = True
            elif args[i] == '-l':
                making_model = False

    data_paths = [str(p) for p in pathlib.Path('data').iterdir()]
    out_dir = 'out'
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
    print("-------------------------", "\n")

    for data_path in data_paths:

        tag_anonymous = False

        print(data_path)
        documents = []
        paths_pure = [p for p in pathlib.Path(data_path).rglob("*/*.html")]
        paths = [str(p) for p in paths_pure]

        named_tags = [str(paths_pure[i].parents[0]).split('_')[1] for i in range(len(paths_pure))]
        # print(paths)

        n_student = len(paths)
        print('N_student: ', n_student)
        for path in paths:
            documents.append(extract_text.get_text_moodle(path))

        out_path = pathlib.PurePath.joinpath(pathlib.Path(out_dir),pathlib.Path(data_path))

        if tag_anonymous is True:
            df_doc = pd.DataFrame([[str(i), documents[i]] for i in range(len(documents))])
        else:
            df_doc = pd.DataFrame([[named_tags[i], documents[i]] for i in range(len(documents))])

        pathlib.Path(out_path).mkdir(exist_ok=True)
        df_doc.to_csv(pathlib.PurePath.joinpath(pathlib.Path(out_path), pathlib.Path("docments.csv")), header=False, index=False)

        pathlib.Path(out_path).mkdir(parents=True, exist_ok=True)
        model_path = pathlib.PurePath.joinpath(pathlib.Path(out_path), pathlib.Path("Doc2vec_similarity.model"))

        model = ''
        words_array = []
    
        if(making_model == True):
            print("Make a models.")
            training_docs = []
            for i, document in enumerate(documents):
                words = extract_text.extract_words(document)
                words_array.append(words)
                # training_docs.append(TaggedDocument(words=words, tags=['doc{0:03d}'.format(i)]))
                if tag_anonymous is True:
                    temp_tag = 'doc{0:03d}'.format(i)
                else:
                    temp_tag = named_tags[i]# str(paths_pure[i].parents[0]).split('_')[1]
                training_docs.append(TaggedDocument(words=words, tags=[temp_tag]))

            max_len = max([len(word) for word in words_array])
            words_array = [ word + [''] * (max_len - len(word)) for word in words_array]
            df = pd.DataFrame(words_array)
            print(df.shape)
            df.to_csv(pathlib.PurePath.joinpath(pathlib.Path(out_path), pathlib.Path("words.csv")),sep=',', encoding='utf-8' )

            model = Doc2Vec(documents=training_docs,
                            alpha=0.025, min_count=5,
                            vector_size=100, epochs=20, workers=4)    
            model.save(str(model_path))
        else:
            print("Read a model.")            
            model = Doc2Vec.load(str(model_path))

        tags = OrderedDict() #辞書の繰り返し時による順番を保つ
        if tag_anonymous is True:
            print("Tag's name: Anonymus")
            tag_list = [('doc{0:03d}'.format(i), paths[i]) for i in range(len(paths))]
        else:
            print("Tag's name: Named")            
            tag_list = [(str(paths_pure[i].parents[0]).split('_')[1], paths[i]) for i in range(len(paths))]
        dic = OrderedDict(tag_list)
        tags.update(dic)
        
        sim_matrix = []
        # for k, v in tags.items():
        for k,vv in tags.items():
            # print("\n", "[" + v + "]")

            # items[0]:tag, items[1]:value of similarity 
            sim = [[v for v in items] for items in model.docvecs.most_similar(k,topn=n_student)]
            sim.append([k, 1.000])

            sim.sort(key=lambda x: x[0])
            # 
            # non-normalize(original value)
            # sim = [vals[1] for vals in sim]
            # normalize
            sim = [0.5*(vals[1] + 1) for vals in sim]


            for i in range(len(sim)):
                sim[i] = round(sim[i], 3)
            # print(sim)
            sim_matrix.append(sim)
        # print(sim_matrix, "\n")
        print('Finished')
        print('----------------')

        csv_path = pathlib.PurePath.joinpath(pathlib.Path(out_path), pathlib.Path("Doc2vec_similarity_matrix.csv"))
        np.savetxt(csv_path, np.array(sim_matrix), delimiter=",", header=",".join(paths))



if __name__ == "__main__":
    main()