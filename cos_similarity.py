# -*- coding: utf-8 -*-

import numpy as np
import MeCab
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import csv
import pathlib

import extract_text


def set_vertcies(documents):

    docs = np.array(documents)
    vectorizer = TfidfVectorizer(
        analyzer=extract_text.extract_words,
        min_df=1,
        token_pattern='(?u)\\b\\w+\\b'
    )
    vecs = vectorizer.fit_transform(docs)
    return vecs.toarray()

def main():
    p_temp = pathlib.Path('data')
    data_paths = [str(p) for p in p_temp.iterdir()]
    print(data_paths)
    out_dir = 'out'
    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)

    for data_path in data_paths:
        documents = []
        # data_path = "./data/HI2 2020-Report01-5-50327/"
        paths = [str(p) for p in pathlib.Path(data_path).rglob("*/*.html")]

        print(len(paths))
        for path in paths:
            documents.append(extract_text.get_text_moodle(path))

        # Cos類似度
        cs_array = cosine_similarity(set_vertcies(documents), set_vertcies(documents))
        # print(cs_array)


        # for i in range(len(cs_array)):
        #     print(paths[i])
        #     print(*[round(cs,4) for cs in cs_array[i]], sep=', ')
        #     print('')

        out_path = pathlib.PurePath.joinpath(pathlib.Path(out_dir),pathlib.Path(data_path))
        print(out_path)
        pathlib.Path(out_path).mkdir(parents=True, exist_ok=True)
        model_path = pathlib.PurePath.joinpath(pathlib.Path(out_path), pathlib.Path("cos_similarity_matrix.csv"))
        np.savetxt(model_path, cs_array, delimiter=",", header=",".join(paths))

if __name__ == "__main__":
    main()