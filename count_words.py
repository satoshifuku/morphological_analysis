import pathlib
import pandas as pd
import numpy as np
import collections

base_path = pathlib.Path('./out/data/')
paths_words = [str(p) for p in base_path.rglob("*/words.csv")]
print(paths_words)

for path in paths_words:
    print('\n', path)
    df = pd.read_csv(path, index_col=0)

    words_np = np.ravel(df.values)
    words = [word for word in words_np if type(word) is str]

    collected_words = collections.Counter(words)
    most_commons = collected_words.most_common(20)
    print(most_commons)
    
    csv_path = pathlib.PurePath.joinpath(pathlib.Path(path).parent, pathlib.Path("word_frequency.csv"))
    np.savetxt(csv_path, np.array(most_commons), fmt="%s", delimiter=",", encoding='utf-8')
