# -*- coding: utf-8 -*-

# import os
import pathlib
import numpy as np
import matplotlib
import matplotlib.pyplot as plt


def draw_heatmap(matrix,fname):
    fig, ax = plt.subplots()

    # l_triangle_matrix = np.tril(matrix)
    # im = ax.imshow(l_triangle_matrix, cmap='Blues')
    im = ax.imshow(matrix, cmap='Blues')

    nn = len(matrix[0])
    ax.set_xticks(np.arange(nn))
    ax.set_yticks(np.arange(nn))

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    # for i in range(nn):
    #     for j in range(nn):
    #         text = ax.text(j, i, matrix[i, j], ha="center", va="center", color="w")

    fig.tight_layout()
    print("Save to: " + str(fname))
    plt.savefig(str(fname)) 


def main():
    
    paths = [str(p) for p in pathlib.Path('out').rglob("*/*matrix.csv")]
    print(paths)

    for path in paths:
        # print(pathlib.Path(path).parents[0])
        fname = pathlib.Path(str(pathlib.Path(path)).replace('.csv', '.png'))
        # print(fname)

        np_array = np.loadtxt(path, delimiter=',', skiprows=1)
        draw_heatmap(np_array,fname)

if __name__ == "__main__":
    main()