import re
import glob
from bs4 import BeautifulSoup# -*- coding: utf-8 -*-

import MeCab


blacklist = ['[document]','noscript','header','html','meta','head','input','script']


def get_text_moodle(path):
    soup = BeautifulSoup(open(path, encoding="utf-8"), "html.parser")
    texts = soup.findAll(text=True)

    # Extract only texts
    texts = [text.strip() for text in texts if text not in blacklist]
    texts = [text for text in texts if text != '']

    joined_text = ''.join(texts)

    # print(joined_text)
    # print("---------------")
    return joined_text


def extract_words(text):
    out_words = []
    tagger = MeCab.Tagger('-Ochasen')
    tagger.parse('')
    node = tagger.parseToNode(text)
    regular_char = r"[\[\]\\!-/:-@[-`{-~.p]"
    while node:
        word_type = node.feature.split(",")[0]
        independent = node.feature.split(",")[1]
        text = node.surface
        if word_type in ["名詞"] and independent not in ['非自立']:
            if re.match(regular_char, text) == None:
                out_words.append(text)
        # elif node.feature.split(",")[0] == u"形容詞":
        #     out_words.append(node.feature.split(",")[6])
        # elif word_type in ["動詞"]:
        #     out_words.append(node.feature.split(",")[6])
        node = node.next
    # print(out_words)
    # print('N words:' + str(len(out_words)))
    return out_words
    

def main():
    r_path = "./data/HI2 2020-Report01-1-50317/*/*.html"
    paths = glob.glob(r_path)
    print(paths)
    for path in paths:
        get_text_moodle(path)


if __name__ == "__main__":
    main()